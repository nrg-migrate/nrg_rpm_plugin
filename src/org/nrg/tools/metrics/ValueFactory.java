package org.nrg.tools.metrics;
/*
 * XNAT http://www.xnat.org
 * Copyright (c) 2013, Washington University School of Medicine
 * All Rights Reserved

  Released under the Simplified BSD.

  Last modified 7/10/13 8:47 PM
 */

import java.util.Map;

import com.newrelic.metrics.publish.Agent;
import com.newrelic.metrics.publish.AgentFactory;

public class ValueFactory extends AgentFactory {

	public ValueFactory() {
		super("org.nrg.tools.metrics.json");
	}
	
	@Override
	public Agent createConfiguredAgent(Map<String, Object> properties) {

        // get the configuration information from the file config/org.nrg.tools.metrics.json
        // unfortunately, we have to resort to this name2 baloney due to the weirdness of the way the newrelic engine
        // seems to work.
		String name = (String) properties.get("name");
		String rpmname = (String) properties.get("rpmname");
        String scriptpath = ((String) properties.get("scriptpath"));
		String name2 = (String) properties.get("name2");
		String rpmname2 = (String) properties.get("rpmname2");
        String scriptpath2 = ((String) properties.get("scriptpath2"));

		return new org.nrg.tools.metrics.RPMExtraReporting(name, rpmname, scriptpath, name2, rpmname2, scriptpath2);
	}
}
