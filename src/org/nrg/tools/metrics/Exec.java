package org.nrg.tools.metrics;
/*
 * XNAT http://www.xnat.org
 * Copyright (c) 2013, Washington University School of Medicine
 * All Rights Reserved

  Released under the Simplified BSD.

  Last modified 7/10/13 8:47 PM
 */

import java.io.*;

/**
 * Support class that will launch a script, then read in the output of the script and return it as a string.
 */
public class Exec {
    public static String GetValues(String script) {
        String output = "";
        try {
            String line;

            Process p = Runtime.getRuntime().exec(script);
            BufferedReader stdoutreader = new BufferedReader
                    (new InputStreamReader(p.getInputStream()));
            BufferedReader stderrreader = new BufferedReader
                    (new InputStreamReader(p.getErrorStream()));
            while ((line = stdoutreader.readLine()) != null) {
                output += line;
//                System.out.println(line);
            }
            stdoutreader.close();
            while ((line = stderrreader.readLine()) != null) {
//                System.out.println(line);
            }
            stderrreader.close();
            p.waitFor();
        }
        catch (Exception err) {
            err.printStackTrace();
        }
        return(output);
    }
}