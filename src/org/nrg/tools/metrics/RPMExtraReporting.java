package org.nrg.tools.metrics;
/*
 * XNAT http://www.xnat.org
 * Copyright (c) 2013, Washington University School of Medicine
 * All Rights Reserved

  Released under the Simplified BSD.

  Last modified 7/10/13 8:47 PM
 */

import com.newrelic.metrics.publish.Agent;
import com.newrelic.metrics.publish.processors.EpochCounter;
import com.newrelic.metrics.publish.processors.Processor;

import java.io.File;

public class RPMExtraReporting extends Agent {
	private String name = "Default";
    private String scriptpath = "";
    private String rpmname = "";
	private String name2 = "Default";
    private String scriptpath2 = "";
    private String rpmname2 = "";

    public RPMExtraReporting(String name, String rpmname, String scriptpath, String name2, String rpmname2, String scriptpath2) {
    	super("org.nrg.tools.metrics.cnda_plugin2", "1.0.0");
    	this.name = name;
        this.scriptpath = scriptpath;
        this.rpmname = rpmname;
    	this.name2 = name2;
        this.scriptpath2 = scriptpath2;
        this.rpmname2 = rpmname2;

        // check to see if the scripts are readable and exectuable
        File f1 = new File(this.scriptpath);
        if (f1.exists() && f1.canExecute()){
        } else {
            System.err.println("Error finding script " + this.scriptpath);
            System.exit(-1);
        }
        if (null != this.scriptpath2){
            File f2 = new File(this.scriptpath2);
            if (f2.exists() && f2.canExecute()){
            } else {
                System.err.println("Error finding script " + this.scriptpath2);
                System.exit(-1);
            }
        }
    }
    
	@Override
	public String getComponentHumanLabel() {
		return "CNDA System Metrics";
	}

    /**
     * This method gets executed once per minute.  It launches the scripts, gathers the values, and sends them up to the RPM servers.
     */
	@Override
	public void pollCycle() {

        if (null != this.scriptpath){
            String nfsval = Exec.GetValues(this.scriptpath);
            // convert the string to a number
            Float num = Float.parseFloat(nfsval);
//            System.out.println("Num Value for " + this.rpmname + " from script " + this.scriptpath+" was: " + nfsval);
            reportMetric(this.rpmname, "value", num);
        }
        if (null != this.scriptpath2){
            String nfsval2 = Exec.GetValues(this.scriptpath2);
            // convert the string to a number
            Float num2 = Float.parseFloat(nfsval2);
//            System.out.println("Num Value for " + this.rpmname2 + " from script " + this.scriptpath2+" was: " + nfsval2);
            reportMetric(this.rpmname2, "value", num2);
        }
	}
	
}

