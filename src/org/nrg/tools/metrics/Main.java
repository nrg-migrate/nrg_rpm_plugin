package org.nrg.tools.metrics;

/*
 * XNAT http://www.xnat.org
 * Copyright (c) 2013, Washington University School of Medicine
 * All Rights Reserved

  Released under the Simplified BSD.

  Last modified 7/10/13 8:47 PM
 */

import com.newrelic.metrics.publish.Runner;
import com.newrelic.metrics.publish.configuration.ConfigurationException;

/**
 * Main class for the RPM Metrics gathering tool.
 */

public class Main {	
    public static void main(String[] args) {
    	Runner runner = new Runner();
    	
    	runner.add(new org.nrg.tools.metrics.ValueFactory());
    	
		try {
	    	//Never returns
	    	runner.setupAndRun();
		} catch (ConfigurationException e) {
			e.printStackTrace();
    		System.err.println("Error configuring");
    		System.exit(-1);
		}
    	
    }
    
}
