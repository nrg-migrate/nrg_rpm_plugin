NRG New Relic RPM Plugin
========================================

The NRG New Relic RPM plugin was created by the Neuroinformatics Research Group at the Washington University School of Medicine. The purpose
of this plugin is to monitor wait state and latency for traffic to NFS-mounted network resources.


Prerequisites
----------------------------------

You should already have the New Relic RPM agent configured and running on your server.


Building the NRG RPM plugin service
----------------------------------

To build the service:

1. Pull the code from https://bitbucket.org/nrg/nrg_rpm_plugin.
2. Build the plugin:
    ant
3. If you are building a server other than the one on which you wish to install the plugin, copy or transfer the file <b>dist/nrg-rpm-plugin-1.0.0.tar.gz</b> to your target server.

Once you have this archive on your target server, you can proceed with the installation process.


Installing the NRG RPM plugin service
----------------------------------

1. Untar the file into the directory <b>/usr/share/tomcat6/newrelic</b> on the target machine.  This should create a directory structure as follows:

        nrg-rpm-plugin-1.0.0/
        nrg-rpm-plugin-1.0.0/config/
        nrg-rpm-plugin-1.0.0/config/newrelic.properties
        nrg-rpm-plugin-1.0.0/config/template_newrelic.properties
        nrg-rpm-plugin-1.0.0/config/example_logging.properties
        nrg-rpm-plugin-1.0.0/config/backup.json
        nrg-rpm-plugin-1.0.0/config/logging.properties
        nrg-rpm-plugin-1.0.0/config/org.nrg.tools.metrics.json
        nrg-rpm-plugin-1.0.0/scripts/nfsreport.sh
        nrg-rpm-plugin-1.0.0/scripts/nrg-rpm-plugin
        nrg-rpm-plugin-1.0.0/nrg-rpm-plugin-1.0.0.jar
        nrg-rpm-plugin-1.0.0/scripts/iowait.sh

2. Copy the <b>nrg-rpm-plugin-1.0.0/nrg-rpm-plugin</b> script to the <b>/etc/init.d</b> directory.  This is the service wrapper for starting and stopping the plugin. The plugin was tested on lab-agent01, so assuming a similar setup to CNDA, the existing paths should be fine.

3.  Check the paths in the following files:

    * <b>/etc/init.d/nrg-rpm-plugin</b>: Make sure the SCRIPT_PATH variable is correct.
    * <b>/usr/share/tomcat6/newrelic/nrg-rpm-plugin-1.0.0/config/org.nrg.tools.metrics.json</b>: Ensure that the paths to the executable scripts are correct.

    Assuming a similar directory structure to lab-agent01, everything should be good to go, but it's worth double checking.

4.  Run the plugin:

        service nrg-rpm-plugin start  
 
    There are a few lines of output on the start, but it's generally nothing important.  It does check to make sure the scripts exist in the specified locations and errors and exits if they're not found.


What It Does
----------------------------------

Every 60 seconds, the plugin wakes up, runs the two scripts specified in the org.nrg.tools.metrics.json file, and sends those values (under the specified names) to the RPM server, which records them.  You can modify the scripts as the tool is running, but if you modify the metrics names you will need to restart the plugin.  There are dashboards configured in RPM that are used to display the data. 


Source Code
-----------

This plugin can be found at https://bitbucket.org/nrg/nrg_rpm_plugin.

